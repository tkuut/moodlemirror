import os
import re

import requests
import requests_cache
from bs4 import BeautifulSoup


class Moodle:

    # set up cache so moodle isn't pinged every time
    requests_cache.install_cache('moodle_cache', expire_after=300)
    
    loginurl = "https://moodle.ut.ee/login/index.php"

    # set up moodle session on object init
    def __init__(self):

        try:
            with open("credentials") as f:
                userpass = f.read().split(":")
                self.username = userpass[0]
                self.password = userpass[1].strip()
        except:
            raise Exception("Puudub kasutajanimi/parool. Palun tekita fail 'credentials', kus on kirjas kasutajanimi:parool.")
    
    def login(self):
        self.session = requests.Session()
        #get token & set MoodleSessionMDL3
        res = self.session.get(self.loginurl)

        logintoken = BeautifulSoup(res.content, "html.parser").find("input",{"name" : "logintoken"}).get('value')

        #auth
        self.session.post(self.loginurl, data = {
            "username" : self.username, 
            "password" : self.password, 
            "logintoken" : logintoken, 
            })

if __name__== "__main__":
    m = Moodle()
    m.login()
    
    # directories for saving items from moodle
    downloads_dir = "downloads/"
    grades_dir = "grades/"
    
    # instantiate main page and collect available courses
    main_page = BeautifulSoup(m.session.get("https://moodle.ut.ee/my/").content, 'html.parser')
    courseboxes = main_page(class_="col-sm-8 span8")

    # set up regex matches for later use
    match_resource = re.compile("resource")
    match_linkset = re.compile("linkset")
    match_linkset_file = re.compile("mod_linkset/file")
    
    # loop over courses
    for c in courseboxes:
        # find link for each of the course and determine names of courses
        coursecontent = c.find("a")
        coursename = coursecontent.contents[0]
        coursedirectory = coursename + '/'
        
        # create directory for downloads
        try:
            os.mkdir(downloads_dir + coursedirectory)
        except Exception as e:
            pass
        
        # init page of a particular course
        subpage = BeautifulSoup(m.session.get(coursecontent['href']).content, 'html.parser')
        activity_instances = subpage(class_="activityinstance")
        
        for instance in activity_instances:
            try:
                activity_link = instance.a['href']
                # get all the materials uploaded moodle resources
                if match_resource.search(activity_link):
                    resource_page = BeautifulSoup(m.session.get(activity_link).content, 'html.parser')
                    resource_instance = resource_page(class_="resourceworkaround")
                    resource_link = resource_instance[0].a['href']
                    download = m.session.get(resource_link)
                    content_disposition = download.headers['content-disposition'].encode("latin_1").decode("utf_8")
                    filename = re.findall("filename=(.+)", content_disposition)[0][1:-1]
                    with open(downloads_dir + coursedirectory + filename, 'wb') as f:
                        f.write(download.content)
                # get all the materials uploaded as links (e.g. engineering graphics)
                elif match_linkset.search(activity_link):
                    linkset_page = BeautifulSoup(m.session.get(activity_link).content, 'html.parser')
                    linkset_instances = linkset_page(class_="menuitemlabel")
                    for linkset_instance in linkset_instances:
                        if match_linkset_file.search(linkset_instance['href']):
                            download_file = linkset_instance['href']
                            download = m.session.get(download_file)
                            content_disposition = download.headers['content-disposition'].encode("latin_1").decode("utf_8")
                            filename = re.findall("filename=(.+)", content_disposition)[0][1:-1]
                            with open(downloads_dir + coursedirectory + filename, 'wb') as f:
                                f.write(download.content)                                
            except Exception as e:
                pass
    
    # init grades page and links
    grades_page = BeautifulSoup(m.session.get("https://moodle.ut.ee/grade/report/overview/index.php").content, 'html.parser')
    grades_anchors = grades_page.find(id="overview-grade").find_all("a")
    # get grades table for each of grade pages
    for anchor in grades_anchors:
        coursename = anchor.string
        link = anchor['href']
        course_grades_page = BeautifulSoup(m.session.get(link).content, 'html.parser')
        grade_table = course_grades_page.find("table", class_="user-grade")
        with open(grades_dir + coursename + '.html', 'wb') as f:
            f.write(grade_table.prettify(formatter="html").encode())
